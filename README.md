# Voidlinux Scripts


## Getting started

These scripts are meant to make life easier when using Voidlinux

Remember you using this at your own risk. These are not official scripts please don't ask Voidlinux team for help.

Script must be run as root!

## Steps Voidlinux 15 (64 bit)

Open up a terminal (be root) and run 

- git clone https://gitlab.com/gosh-its-arch-linux/voidlinux.git
- cd voidlinux
- cd scripts
- chmod +x *.sh


Once completed you should be able to run the individual scripts (All must be run as root)

## What each file does

- update_voidlinux -> Use this script to automatically update Voidlinux

- gnome_and_audio_setup -> Install Gnome Desktop Enviroment and PulseAudio from a base system. Also sets the symbolic links automatically so Gnome boots up automatically.

- kde_installer -> Install KDE Desktop Enviromentfrom a base system. Also sets the symbolic links automatically so KDE boots up automatically.
